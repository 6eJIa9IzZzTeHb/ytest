<?php
namespace backend\models;


use common\models\Promo;
use common\models\PromoToUser;
use common\models\User;
use yii\base\Model;

class ActivatePromoForm extends Model {
    public $user_id;
    public $promo_id;



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'Пользователь',
            'promo_id' => 'Промо',
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'promo_id'], 'required'],
            ['promo_id', 'checkNotExists']
        ];
    }

    public function checkNotExists() {

        /**
         * @var $promo Promo
         */
        $promo = Promo::findOne(['id'=>$this->promo_id]);
        $active = $promo->status;

        if (time() > strtotime($promo->finish_at) + 24 * 60 * 60 || time() < strtotime($promo->start_at)) {
            $active = false;
        }

        if (!$active) {
            $this->addError('promo_id', 'Промо-код недействителен');
        }

        //if user already has promo
        if (PromoToUser::findOne([
            'user_id'=>$this->user_id,
            'promo_id'=>$this->promo_id
        ]) != null) {
            $this->addError('promo_id', 'Пользовтель уже воспосльзовался промо кодом');
        }
    }


    public function finish() {
        /**
         * @var $promo Promo
         * @var $user User
         */
        $promo = Promo::findOne(['id'=>$this->promo_id]);
        $user = User::findIdentity($this->user_id);




        //change balance
        $user->balance += floatval($promo->profit);
        $user->save();


        //create
        $promoToUser = new PromoToUser();
        $promoToUser->user_id = $this->user_id;
        $promoToUser->promo_id = $this->promo_id;
        $promoToUser->save();

    }

}


