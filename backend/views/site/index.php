<?php

/* @var $this yii\web\View */

$this->title = 'Главная';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Приветствуем!</h1>

        <p class="lead">Вы вошли в административную панель.</p>
        <p class="lead">Что дальше?</p>

        <p>
            <a class="btn btn-lg btn-success" href="<?= \yii\helpers\Url::to(['city/index'])?>">Города</a>
            <a class="btn btn-sm btn-warning" href="<?= \yii\helpers\Url::to(['promo/index'])?>">Промо</a>
            <a class="btn btn-lg btn-info" href="<?= \yii\helpers\Url::to(['site/stat'])?>">Статистика</a>
        </p>
    </div>

</div>
