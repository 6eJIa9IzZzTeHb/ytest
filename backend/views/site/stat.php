<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model \backend\models\ActivatePromoForm */
/* @var $searchModel \backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Статистика';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="site-stat">






    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = \yii\widgets\ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->widget(kartik\select2\Select2::className(), [
        'data' => \common\models\User::getAllAsList(),
        'options' => [
            'placeholder' => 'Выберите пользователя ...',
            'maximumSelectionSize' => 3
        ],
    ]); ?>


    <?= $form->field($model, 'promo_id')->widget(kartik\select2\Select2::className(), [
        'data' => \common\models\Promo::getAllAsList(),
        'options' => [
            'placeholder' => 'Выберите гоорода ...',
            'maximumSelectionSize' => 3
        ],
    ]); ?>

    <button class="btn btn-success">Применить промо</button>

    <?php \yii\widgets\ActiveForm::end(); ?>


    <br/><br/>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            'balance',
            [
                'attribute' => 'promos',
                'format' => 'html',
                'value' => function ($model, $key, $index, $column) {
                     /**
                     * @var $model \common\models\User
                     */


                    $tags = [];
                    foreach ($model->getPromos() as $promo) {
                        $tags []= Html::a($promo->name, [
                           'promo/view', 'id'=>$promo->id
                        ]);
                    }


                    return implode($tags, ', ');
                }
            ],
        ],
    ]); ?>
</div>