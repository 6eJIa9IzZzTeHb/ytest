<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PromoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Промо';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="promo-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Новый промо-код', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'profit',
            [
                'attribute' => 'cities',
                'value' => function ($model, $key, $index, $column) {
                    $ids = $model->cities;
                    $cities = [];
                    $citiesNames = \common\models\City::getAllAsList();
                    foreach ($ids as $id) {
                        $cities []= $citiesNames[$id];
                    }

                    return implode($cities, ", ");
                }
            ],
            [
                'attribute' => 'start_at',
                'format' => ['date', 'php:d.m.Y']
            ],
            [
                'attribute' => 'finish_at',
                'format' => ['date', 'php:d.m.Y']
            ],
            // 'status',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
