<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Promo */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Промо', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'profit',
            'profit',
            [
                'attribute' => 'cities',
                'value' => function ($model) {
                    $ids = $model->cities;
                    $cities = [];
                    $citiesNames = \common\models\City::getAllAsList();
                    foreach ($ids as $id) {
                        $cities []= $citiesNames[$id];
                    }

                    return implode($cities, ", ");
                }
            ],
            [
                'attribute' => 'start_at',
                'format' => ['date', 'php:d.m.Y']
            ],
            [
                'attribute' => 'finish_at',
                'format' => ['date', 'php:d.m.Y']
            ],
            'status',
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:H:i:s d.m.Y']
            ],
            [
                'attribute' => 'updated_at',
                'format' => ['date', 'php:H:i:s d.m.Y']
            ],
        ],
    ]) ?>

</div>
