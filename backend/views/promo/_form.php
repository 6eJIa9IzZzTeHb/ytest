<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Promo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="promo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'profit')->widget(\kartik\money\MaskMoney::className(), [
            'value' => 0.00,
            'pluginOptions' => [
                'prefix' => '$ ',
                'thousands' => '.',
                'decimal' => ',',
                'precision' => 2
            ],
        ])
    ?>

    <?= $form->field($model, 'cities')->widget(\kartik\select2\Select2::className(), [
        'data' => \common\models\City::getAllAsList(),
        'options' => [
            'placeholder' => 'Выберите гоорода ...',
            'multiple' => true,
            'maximumSelectionSize' => 3
        ],
    ]) ?>

    <?= $form->field($model, 'start_at')->widget(\kartik\date\DatePicker::className(),[
            'value' => date('d.m.Y', strtotime('+0 days')),
            'options' => ['placeholder' => 'Select issue date ...'],
            'pluginOptions' => [
                'format' => 'dd.mm.yyyy',
                'todayHighlight' => true
            ]
    ]) ?>

    <?= $form->field($model, 'finish_at')->widget(\kartik\date\DatePicker::className(),[
        'value' => date('d.m.Y', strtotime('+2 days')),
        'options' => ['placeholder' => 'Select issue date ...'],
        'pluginOptions' => [
            'format' => 'dd.mm.yyyy',
            'todayHighlight' => true
        ]
    ]) ?>

    <?= $form->field($model, 'status')->dropDownList([
        "1"=>'Активный',
        "2"=>'Закрытый',
    ]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
