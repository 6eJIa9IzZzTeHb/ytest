<?php
namespace api\controllers;

use common\models\City;
use common\models\Promo;
use common\models\PromoToUser;
use common\models\User;
use Prophecy\Exception\Doubler\MethodNotExtendableException;
use Yii;
use yii\base\Exception;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }



    public function actionIndex() {
        throw new MethodNotAllowedHttpException("Не был передан метод");
    }

    public function actionTest() {

        return $this->render('answer', [
            'status'=>'ok',
            'data'=>[]
        ]);
    }

    public function actionGetDiscountInfo($name, $fields = ['start_at', 'finish_at', 'profit', 'cities', 'status']) {


        /**
         * @var $promo Promo
         */
        $promo = Promo::findOne(['name'=>$name]);

        $data = [];

        if ($promo) {
            foreach ($fields as $field) {
                switch ($field) {
                    case 'cities':
                        $ids = $promo->cities;
                        $cities = [];
                        $citiesNames = City::getAllAsList();
                        foreach ($ids as $id) {
                            $cities [] = $citiesNames[$id];
                        }

                        $data[$field] = implode($cities, ", ");
                        break;
                    case 'status':
                        $active = $promo->status;

                        if (time() > strtotime($promo->finish_at) + 24 * 60 * 60 || time() < strtotime($promo->start_at)) {
                            $active = false;
                        }

                        $data[$field] = $active ? 'active' : 'closed';
                        break;

                    default:
                        $data[$field] = $promo->$field;
                }
            }
        }



        return $this->render('answer', $promo?[
            'status'=>'ok',
            'data'=>$data
        ]: [
            'status'=>'error',
            'data'=>'Промо не найдено'
        ]);
    }


    public function actionActivateDiscount($user_name, $promo_name, $zone) {
        /**
         * @var $promo Promo
         * @var $user User
         */
        $promo = Promo::findOne(['name'=>$promo_name]);
        $user = User::findOne(['username'=>$user_name]);


        $error = [];
        if ($promo) {

            $active = $promo->status;

            if (time() > strtotime($promo->finish_at) + 24 * 60 * 60 || time() < strtotime($promo->start_at)) {
                $active = false;
            }

            $ids = $promo->cities;
            $cities = [];
            $citiesNames = City::getAllAsList();
            foreach ($ids as $id) {
                $cities [] = $citiesNames[$id];
            }

            if (!$active) {
                $error []= "Промо-код недействителен";
            }

            if (!in_array($zone, $cities)) {
                $error []= "Промо-код не подходит выбранной зоне";
            }


        }


        if (!$user) {
            $error []= "Пользователь не найден";
        }

        if (!$promo) {
            $error []= "Промо-код не найден";
        }

        if ($user && PromoToUser::findOne(['user_id'=>$user->id, 'promo_id'=>$promo->id])) {
            $error []= "Промо-код уже активирован";
        }

        if (!$error) {
            //change balance
            $user->balance += floatval($promo->profit);
            $user->save();


            //create
            $promoToUser = new PromoToUser();
            $promoToUser->user_id = $user->id;
            $promoToUser->promo_id = $promo->id;
            $promoToUser->save();

            return $this->render('answer', [
                'status'=>'ok',
                'data'=>[
                    'profit'=>$promo->profit
                ]
            ]);
        }

        return $this->render('answer', [
            'status'=>'error',
            'data'=>$error
        ]);
    }
}
