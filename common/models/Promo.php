<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "promo".
 *
 * @property integer $id
 * @property string $name
 * @property float $profit
 * @property integer $start_at
 * @property integer $finish_at
 * @property integer $status
 * @property string|array $cities
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property PromoToUser[] $promoToUsers
 */
class Promo extends \yii\db\ActiveRecord
{
    public static $all = null;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promo';
    }

    public function beforeSave($insert) {
        $this->cities = json_encode($this->cities);
        $this->start_at = strtotime($this->start_at);
        $this->finish_at = strtotime($this->finish_at);

        return parent::beforeSave($insert);
    }

    public function afterFind() {
        $this->cities = json_decode($this->cities);
        $this->start_at = date("d.m.Y", $this->start_at);
        $this->finish_at = date("d.m.Y", $this->finish_at);
        $this->profit = number_format($this->profit, 4, '.', '');


        parent::afterFind();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'start_at', 'finish_at', 'cities'], 'required'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            ['name', 'match', 'pattern'=>'/[a-zA-Z_\d]{3,10}/', 'message'=>'Должно состоять из латинских букв, подчёркиваний и цифр. Длина от 3 до 10 символов'],
            ['profit', 'match', 'pattern'=>'/\d{1,3}(,\d{3})*(.\d{2})?/', 'message'=>'Должно быть корректной ценой'],
            ['cities', 'checkArrayLength']
        ];
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'profit' => 'Прибыль',
            'start_at' => 'Начало',
            'finish_at' => 'Конец',
            'status' => 'Статус',
            'cities' => 'Тарифная зона',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromoToUsers()
    {
        return $this->hasMany(PromoToUser::className(), ['promo_id' => 'id']);
    }

    public function checkArrayLength() {
        if (sizeof($this->cities) > 5 || sizeof($this->cities) < 0) {
            $this->addError('cities', 'Должно быть от 1 до 5 городов');
        }
    }



    public static function getAllAsList() {
        if (self::$all) {
            return self::$all;
        }

        return self::$all = ArrayHelper::map(self::find()->all(),'id', 'name');
    }
}
