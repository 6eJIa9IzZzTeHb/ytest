<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "promo_to_user".
 *
 * @property integer $user_id
 * @property integer $promo_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Promo $promo
 * @property User $user
 */
class PromoToUser extends \yii\db\ActiveRecord
{
    public static $all = null;
    public static $allToUser = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promo_to_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'promo_id'], 'required'],
            [['user_id', 'promo_id'], 'integer'],
            [['promo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Promo::className(), 'targetAttribute' => ['promo_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'promo_id' => 'Promo ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromo()
    {
        return $this->hasOne(Promo::className(), ['id' => 'promo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


    public static function getAllAsList() {
        if (self::$all) {
            return self::$all;
        }

        return self::$all = self::find()->all();
    }


    public static function getAllToUser($id) {
        if (!self::$allToUser) {

            self::$allToUser = [];
            foreach (User::getAllAsList() as $uid => $name) {
                self::$allToUser[$uid] = [];
            }

            $promos = [];
            foreach (Promo::find()->all() as $promo) {
                $promos[$promo->id] = $promo;
            }
            foreach (PromoToUser::getAllAsList() as $ptu) {
                self::$allToUser[$ptu->user_id] []= $promos[$ptu->promo_id];
            }
        }

        return self::$allToUser[$id];
    }
}
