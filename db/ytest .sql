-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 05 2017 г., 03:08
-- Версия сервера: 5.6.34
-- Версия PHP: 7.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `ytest`
--

-- --------------------------------------------------------

--
-- Структура таблицы `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `city`
--

INSERT INTO `city` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Запорожье', 1509782955, 1509782955),
(2, 'Днепропетровск', 1509782967, 1509782967),
(3, 'Одесса', 1509782995, 1509782995),
(5, 'Киев', 1509783016, 1509783016),
(6, 'Никополь', 1509783025, 1509783025),
(7, 'Москва', 1509783031, 1509783031),
(8, 'Львов', 1509783038, 1509783038);

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1509776215),
('m130524_201442_init', 1509776217),
('m171104_055822_promo', 1509782859),
('m171104_055831_promo_to_user', 1509782860),
('m171104_061132_city', 1509782860);

-- --------------------------------------------------------

--
-- Структура таблицы `promo`
--

CREATE TABLE `promo` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `profit` decimal(12,4) DEFAULT NULL,
  `cities` varchar(255) DEFAULT NULL,
  `start_at` int(11) NOT NULL,
  `finish_at` int(11) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `promo`
--

INSERT INTO `promo` (`id`, `name`, `profit`, `cities`, `start_at`, `finish_at`, `status`, `created_at`, `updated_at`) VALUES
(2, 'aaa', '2002.2200', '[\"1\",\"2\",\"3\",\"5\",\"6\"]', 1509742800, 1509829200, 1, 1509815134, 1509816296),
(3, 'bbbb', '20.0000', '[\"1\",\"2\",\"3\",\"5\",\"6\"]', 1509742800, 1510952400, 1, 1509816450, 1509839077);

-- --------------------------------------------------------

--
-- Структура таблицы `promo_to_user`
--

CREATE TABLE `promo_to_user` (
  `user_id` int(11) NOT NULL,
  `promo_id` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `promo_to_user`
--

INSERT INTO `promo_to_user` (`user_id`, `promo_id`, `created_at`, `updated_at`) VALUES
(1, 2, 1509826824, 1509826824),
(3, 3, 1509830023, 1509830023),
(1, 3, 1509839140, 1509839140),
(4, 3, 1509839661, 1509839661);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `balance` decimal(12,4) DEFAULT '0.0000',
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `balance`, `status`, `created_at`, `updated_at`) VALUES
(1, 'rsemscom', 'SVqAXjKg8lYpYJqFJvbY5v6KaRQdek1y', '$2y$13$1PJ61JkXSAxSI2hIgaxf9.WXgKkwS/GMJbl3.W9RVTmJjLcK0JWTO', NULL, 'rsemscom@yandex.ru', '2042.2200', 10, 1509839140, 1509839140),
(3, 'test59fd5ec680ec2', 'HrxzmpGHjZu7t28kuO5_GrnTRV-MrHU2', '$2y$13$YmHNPb7itu36L2j/dNe9Be..M85NJup4BUT9o0UoJAHdjzMsEOJlq', NULL, 'test59fd5ec680ec2@yandex.ru', '20.0000', 10, 1509830023, 1509830023),
(4, 'test59fd5fae68647', '8kRBs7eUrXgYDzB_92QQRizZ_VnGkfef', '$2y$13$MTgbEdKgfhfDd2VoDg8QoO8ygleMQyfDOVjGO16uCPTxMX8tu4tlS', NULL, 'test59fd5fae68647@yandex.ru', '20.0000', 10, 1509839661, 1509839661);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `promo`
--
ALTER TABLE `promo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `promo_to_user`
--
ALTER TABLE `promo_to_user`
  ADD KEY `fk-promo-to-user-user_id-user-id` (`user_id`),
  ADD KEY `fk-promo-to-user-promo_id-promo-id` (`promo_id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `promo`
--
ALTER TABLE `promo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `promo_to_user`
--
ALTER TABLE `promo_to_user`
  ADD CONSTRAINT `fk-promo-to-user-promo_id-promo-id` FOREIGN KEY (`promo_id`) REFERENCES `promo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk-promo-to-user-user_id-user-id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
