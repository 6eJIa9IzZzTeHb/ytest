<?php

use yii\db\Migration;

/**
 * Class m171104_061132_city
 */
class m171104_061132_city extends Migration
{
    const TABLE_NAME = "city";

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            //cause this can be used like a link
            'name' => $this->string()->notNull()->unique(),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull()
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
