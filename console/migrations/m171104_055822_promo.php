<?php

use yii\db\Migration;

/**
 * Class m171104_055822_promo
 */
class m171104_055822_promo extends Migration
{
    const TABLE_NAME = "promo";

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            //cause this can be used like a link
            'name' => $this->string()->notNull()->unique(),
            'profit' => $this->decimal(12, 4),


            'cities' => $this->string(),


            'start_at' => $this->integer()->notNull(),
            'finish_at' => $this->integer()->notNull(),

            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull()
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
