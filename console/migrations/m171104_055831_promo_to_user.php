<?php

use yii\db\Migration;

/**
 * Class m171104_055831_promo_to_user
 */
class m171104_055831_promo_to_user extends Migration
{
    const TABLE_NAME = "promo_to_user";

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'user_id' => $this->integer()->notNull(),
            'promo_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull()
        ]);
        $this->addForeignKey('fk-promo-to-user-user_id-user-id', self::TABLE_NAME, 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-promo-to-user-promo_id-promo-id', self::TABLE_NAME, 'promo_id', 'promo', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        $this->dropForeignKey('fk-promo-to-user-user_id-user-id', self::TABLE_NAME);
        $this->dropForeignKey('fk-promo-to-user-promo_id-promo-id', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
    }
}
