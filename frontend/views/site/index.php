<?php

/* @var $this yii\web\View */

$this->title = 'Test';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Приветствуем!</h1>

        <p class="lead">Фронтенд не был реализован. Перейдите в панель администрации.</p>

        <p><a class="btn btn-lg btn-success" href="<?= str_replace('//', '//admin.', \yii\helpers\Url::home(true))?>">Вход</a></p>
    </div>

</div>
