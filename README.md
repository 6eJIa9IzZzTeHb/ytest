Ytest 1.0


Installing project:

1) Clone repository

2) Install db. Here you have two ways:

    a) install via console (yii migrate)

    b) install via test DB dump (<app>/db)

   Next you need to configure it in (common/config/main)

3) Use 'composer update' in php console and site folder

4) Configure your oppenserver or root .htaccess

    sitename.com => frontend/web
    api.sitename.com => api/web
    admin.sitename.com => backend/web


5) Be happy!=)

Notes:

API will return the json code

Admin user in test db - rsemscom-rsemscom

To add new users you can use admin.yoursite.com/generate-random-user


Test task


1) Creating the promo code
You can create it due backend app yii2-advanced (admin.yoursite.com/promo/create)

2) Displaying the promo code
You can create it due backend app yii2-advanced (admin.yoursite.com/promo/)

3) Editing promo code
You can create it due backend app yii2-advanced (admin.yoursite.com/update/)

4) API
You can send requests to api app. (api folder)

Data: {
    promo_name: <the name of the promo code>
}

api.yoursite.com/get_discount_info?promo_name=aaa

5) API second method

Data: {
    promo_name: <the name of the promo code>
    user_name: <the name of the user code>
    zone: <Zone name>
}

api.yoursite.com/activate_dicount?user_name=rsemscom&promo_name=aaa&zone=Запорожье




